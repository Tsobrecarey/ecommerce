<?php
	require "../partials/template.php";

	function get_body_contents(){
?>
<h1 class="text-center py-3"></h1>
<hr>
<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<table class="table table-striped">
				<thead>
					<th>Item Name</th>
					<th>Item Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</thead>
				<tbody>
					<?php
						session_start();
						$items=file_get_contents("../assets/lib/products.json");
						$items_array=json_decode($items, true);
						$total=0;

					if(isset($_SESSION['checkout'])){
						foreach ($_SESSION['checkout'] as $brand => $quantity) {
							foreach ($items_array as $indiv_item) {
								if($brand==$indiv_item['brand']){
									$subtotal=$indiv_item['price']*$quantity;
									$total+=$subtotal;
					?>
				<tr>
					<td><?php echo $brand ?></td>
					<td><?php echo $indiv_item['price'] ?></td>
					<td>
						<form action="../controllers/addtocheck-process.php" method="POST">
							<div class="input-group">
								<input type="hidden" name="brand" value="<?php echo $brand ?>">
								<input type="hidden" name="fromcheckout" value="fromcheckout">
								<input type="number" class="form-control" name="quantity" value="<?php echo $quantity ?>">
								<div class="input-group-append">
									<button class="btn btn-sm btn-success" type="submit">Update</button>	
								</div>
							</div>
						</form>
					</td>
					<td><?php echo $subtotal ?></td>
					<td>
						<a href="../controllers/removeitem_process.php?brand=<?php echo $brand ?>" class="btn btn-danger">Remove</a>
					</td>
				</tr>
					<?php
								}
							}
						}
					}
					?>
				<tr class="bg-primary">
					<td></td>
					<td></td>
					<td>Total: </td>
					<td><?php echo number_format($total, 2, ".", ",") ?></td>
					<td>
						<a href="../controllers/emptycheckout_process.php" class="btn btn-danger">Remove All</a>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
	}
?>