<?php
	require "../partials/template.php";
	function get_body_contents(){
?>
	<div class="container">
		<div class="row">
			<?php
				$products=file_get_contents('../assets/lib/products.json');
				$products_array=json_decode($products,true);

				foreach ($products_array as $indiv_product) {
			?>
				<div class="col-lg-4 py-2 mb-5">
							<div class="card h-100">
								<div class="d-flex justify-content-center align-items-center p-3 h-50">
									<img src="../assets/lib/<?php echo $indiv_product['image']?>" class="h-auto"alt="">
								</div>
								<div class="card-body">
									<h5 class="card-title"><?php echo $indiv_product['brand']?></h5>
									<p class="card-text"> Price is Php<?php echo $indiv_product['price']?></p>
									<p class="card-text">Description: <?php echo $indiv_product['description']?></p>
								</div>
								<div class="card-footer text-center d-flex justify-content-center align-items-center">
									<a href="../controllers/deleteitem-process.php?brand=<?php echo $indiv_product['brand']?>" class="btn btn-danger mx-2">Delete Item</a>
								</div>
								<div class="card-footer">
									<form action="../controllers/addtocheck-process.php?brand=<?php echo $indiv_product['brand']?>" class="form-group" method="POST">
									<input type="hidden" name="brand" class="form-control" value="<?php echo $indiv_product['brand']?>">
									<input type="number" name="quantity" value="1" class="form-control">
									<button type="submit" class="btn btn-success btn-block">+ Add To Checkout</button>
									</form>
								</div>
							</div>					
						</div>
			<?php
				}
			?>
		</div>
	</div>
<?php
	}
?>