<?php
	session_start();
	$brand=$_POST['brand'];
	$quantity= intval($_POST['quantity']);

	if(isset($_POST['fromcheckout']) && $quantity <1){
		header("Location: ". $_SERVER['HTTP_REFERER']);
	}else if(isset($_POST['fromcheckout'])){
		$_SESSION['checkout'][$brand] = $quantity;
		header("Location: ". $_SERVER['HTTP_REFERER']);
	}else {
		if(!isset($_SESSION['checkout'][$brand])){
			$_SESSION['checkout'][$brand]=$quantity;
		}else{
			$_SESSION['checkout'][$brand]+=$quantity;
		}

		header("Location: ". $_SERVER['HTTP_REFERER']);
	}
?>